# Ser y tiempo - Martin Heidegger

## Introducción 

> ### La esencia del cristianismo - Ludwig Feuerbach
> 
> Puntos asociados al tema del ser:
> 
> * El ser absoluto el Dios del hombre es la escencia del hombre.
> * Cualquiera que sea el objeto que se presente a nuestra conciencia siempre nos hace conscientes de nuestra propia escencia, sólo activamos una cosa si nos activamos al mismo tiempo.
> * ***La consciencia significa activarse a si mismo, alegría de la propia perfección.***
> * ***La conciencia de Dios es la autoconsciencia del hombre. El conocimiento de Dios es el autoconocimiento del hombre.***
> 
> La religión cristiana surge por el deseo del hombre de idealizar sus perfecciones, de proyectar su escencia, es decir, no es que el hombre esté hecho a la imagen y semejanza de Dios, más bien ***Dios está hecho a la imagen y semejanza del hombre***. La comprensión de Dios es la autocompresión del hombre.

## Desarrollo

Tomando como premisa el *círculo hermenéutico* (Heidegger-Gadamer) y el aforismo ***Nada puede ser querido sin ser antes conocido*** (San Agustín).
La preminencia óntica sería: ***Nosotros vamos a comprender del ser solamente lo que nosotros estemos siendo ya en cada caso siempre en si mismos***. 
El punto de partida para pensar en el ser es ***el cuidado de si mismo (Besorgen)***.

> ## La objetividad entre paréntesis en el aula de Física - Patricia Alexandra Knopoff
> 
> Puntos asociados al tema del ser como observador:
> 
> ### Preludio de Maturana
> * Los seres humanos operan como observadores, haciendo distinciones en el lenguaje.
> * Las explicaciones y descripciones agregan o reafirman lo que explican o describen y son secundarias a la praxis del vivir en el lenguaje del observador, y por lo tanto, innecesarias. Lo esencial es el observar, como punto de partida para cualquier tipo de entendimiento de realidad y razón como fenómenos humanos.
> * Cuando respondemos una pregunta, proponemos una reformulación de una situación de nuestra praxis del vivir con otros elementos de nuestra praxis del vivir y si es aceptada por un oyente, es decir que es aceptada por él como una reformulación de su praxis del vivir, nuestra respuesta será una explicación.
> * Todo lo dicho es dicho por alguien, y un observador acepta o rechaza lo dicho. Si lo dicho es reformulado desde la praxis del vivir del observador oyente, será una explicación.
> 
> ### El primer camino: la objetividad SIN paréntesis
> * ***Es en este camino explicativo donde una pretensión de conocimiento es una demanda de obediencia.***
> 
> ### El segundo camino: la objetividad ENTRE paréntesis
> * En el camino explicativo de la objetividad entre paréntesis el observador se acepta como ser viviente en el que sus habilidades cognitivas tienen fundamento biológico y para quien resultará imposible distinguir, a través de la experiencia, entre percepción e ilusión.
> * El observador acepta que dos explicaciones mutuamente excluyentes, frente a situaciones que para un tercero son la misma, implican solamente que los tres observadores están operando en diferentes dominios explicativos en sus praxis del vivir.
> * Los observadores que comparten un criterio de validación para sus explicaciones operan en dominios cognitivos intersecados.
> * El hecho que un observador opere en un dominio o en otro depende exclusivamente de su preferencia operacional.
> 
> ###  Los científicos y la ciencia
> * La pasión por explicar la praxis del vivir es la emoción fundamental de los científicos.
> * Una explicación científica nunca es una reducción fenoménica, ya que el fenómeno a explicar y el mecanismo explicativo pertenecen a dominios fenoménicos diferentes y disjuntos.
> * Lo que distinguirá a un *observador científico* de un *observador de la vida diaria* será la orientación emocional del científico a explicar, su consistencia para usar solamente el criterio de validación de las explicaciones científicas y su compromiso para clarificar ambos dominios fenoménicos en la generación de explicaciones. 
> * El ser inconciente de las implicancias ontológicas y epistemológicas de su hacer al hacer ciencia implica aceptar la explicación científica como proposición reduccionista y la incapacidad de ver el carácter generativo de la explicación, por creer estar referenciando la explicación a una realidad objetiva, e independiente del hacer del científico. Así, es común encontrar que se confunde ser objetivo en ciencia con la validez referenciada con una realidad independiente.
> * Contrariamente, dentro del camino explicativo de la objetividad entre paréntesis, las explicaciones científicas nunca explican un mundo independiente del observador sino la propia experiencia del observador, que es precisamente el mundo que el observador vive.
> * ***El observador surge en la praxis del vivir en el lenguaje, y él o ella se encuentra a sí mismo o a sí misma en la experiencia de ocurrir como un hecho, anterior a cualquier reflexión o explicación.***
> * En el camino explicativo de la objetividad sin paréntesis, la realidad es una proposición explicativa, constituida como un dominio de entidades que se asume que existen independientemente del hacer del observador, y éste evita preguntarse sobre el origen biológico de sus capacidades cognitivas.
> * Yendo más lejos, la posibilidad de la existencia de los dos caminos explicativos solo puede aparecer desde el camino de la objetividad entre paréntesis, que permite al observador reflexionar sobre implicaciones epistemológicas y ontológicas. Sin embargo, la elección del camino explicativo que tome el observador responde únicamente a sus preferencias, a la emoción de aceptación hacia uno u otro camino, pudiendo transitar alternativamente entre uno y otro de manera inconciente según su flujo emocional.
> 
> ###  La realidad
> * ***La racionalidad es la operación del observador de acuerdo a las coherencias operacionales en el lenguajear, en un dominio particular de realidad.***
> * Cada sistema racional se funda en premisas iracionales y queda especificado con un mínimo de elementos iniciales que determinen un dominio de coherencia operacional.
> * En este camino sucederá que dos observadores en desacuerdo nunca se enemistan sino buscan un dominio de coexistencia en aceptación mutua o bien aceptarán responsablemente sus desacuerdos en un marco de respeto mutuo o de negación responsable. Todo lo que se puede hacer es seducir al interlocutor para que acepte como válidas las premisas básicas que definen el dominio en el cual ese argumento es operacionalmente válido.
> 
> ### El lenguaje y la emoción
> * El ser humano acontece en el lenguaje y es inexietnte posibilidad de referirse a sí mismo fuera del lenguaje.
> * El lenguajear ocurre cuando se observa un tipo peculiar de flujo en las interacciones y coordinaciones de acciones entre seres humanos, en un plano diferente al de la fisiología. El lenguajear nunca es un fenómeno neurofisiológico.
> * El lenguaje surge en el flujo de coordinaciones consensuales de acciones entre organismos que comparten una deriva co-ontogénica.
> * Las emociones son las diferentes maneras de interactuar que se pueden observar en los animales. Toda la vida ocurre bajo un flujo de emociones que cambian los dominios de acciones en los cuales es posible operar.
> * Cuando en un grupo de observadores se distingue un flujo de coordinaciones de acciones en el lenguaje, se distingue una conversación.
> * Lo que distingue al ser humano es su capacidad para vivir en el lenguaje en ***el trenzado constitutivo del lenguajear y emocionar***.
> * La estructura inicial de un organismo hace posible todo lo que puede sucederle en su historia individual y nunca especifica su futuro. Todo lo que ocurre en un sistema viviente acontece como un resultado de sus cambios continuos en una historia de interacciones en un medio, bajo la forma de una epigénesis.
> 
> ### La demanda de obediencia: la objetividad como recurso para obligar
> * El conocimiento es conducta aceptada como adecuada por un observador en un dominio particular que haya especificado.
> * En el camino de la objetividad en paréntesis, las afirmaciones cognitivas son invitaciones a entrar en el mismo dominio de realidad del orador. Los desacuerdos cognitivos omiten la negación del otro y constituyen la posibilidad de una conversación que podría generar un nuevo dominio de realidad donde los desacuerdos desaparezcan y ambos puedan coexistir. En este camino, la dinámica emocional irá a través de la seducción en lugar de la obediencia.
> 
> ### El amor
> * Los fenómenos sociales ocurren cuando dos o más organismos siguen un curso operacional de aceptación mutua, en interacciones recurrentes.
> * La emoción que hace posible estas interacciones es el amor, que es el dominio de acciones mediante el cual los sistemas vivientes coordinan sus acciones de modo de lograr la aceptación mutua.
> * Los sistemas sociales humanos son sistemas de coordinaciones de acciones en el lenguaje y por lo tanto son redes de conversaciones.
> * Los seres humanos se mueven cotidianamente a través de una red de conversaciones, integrando y abandonando sistemas sociales de acuerdo a la aceptación o rechazo a la coexistencia en aceptación mutua que surge del lenguajeo y el emocionar.
> * Las fronteras de los sistemas sociales son emocionales y solo pueden ser traspasadas a través de la seducción emocional y nunca a través de la razón.

## Conclusión

Maturana hace una descripción del mundo. Y dice que ese mundo puede ser mirado a través de dos cristales diferentes, el de la objetividad sin paréntesis y el de la objetividad entre paréntesis. Y más aún, nos aclara que sólo con el cristal de la objetividad entre paréntesis es posible discernir la existencia del otro mundo.

Este mundo existe hace varios siglos. Fue la gran ganancia de la Modernidad, que nos legó la Razón, madre de todas las Verdades y sustento de solución para todos los Males. Pero este mundo niega de algún modo la grandeza del Ser, ya que su existencia es prescindible. Una persona puede existir o no, pero el mundo verdadero seguirá estando allí, inmutable. Le da igual a la Realidad que el hombre exista, le da igual a la Realidad que el hombre intente conocerla, comprenderla, explicarla.

La realidad pasa a ser parte de quien la trae a la mano. Es inexistente una realidad allí fuera, donde sólo algunos privilegiados acceden. Es existente un camino de privilegio para acceder a la Verdad. Es inexistente *La Verdad*. Hay tantas explicaciones como dominios de acción posibles. Y ninguna de ellas tiene mayor valor que otra. Son todas igualmente legítimas. Y estar en desacuerdo solo implica que los dominios son disjuntos, sin embargo siguen siendo legítimos. Cabe siempre la posibilidad de invitar al otro a otear el mundo que se propone; cabe siempre la posibilidad que el otro niegue responsablemente esta invitación. Cabe siempre la posibilidad de consensuar nuevos espacios y co-crear nuevas explicaciones conjuntas consensuadas.

Considero que siempre será más agradable transitar un mundo al cual se ingresa y permanece por seducción e invitación, que aquel mundo en el cual se carece de una conciencia clara del motivo de la permanencia y del modo por el cual se llegó a él.

Maturana nos invita a tomar conciencia. A reflexionar. Y la reflexión se entiende en el doble sentido del pensar y el pensar en sí mismo. El tomar conciencia, el reflexionar, implica necesariamente la responsabilidad sobre lo que se hace y sobre lo que se deja de hacer.

El camino que invita a recorrer es tentador, tanto para uno mismo desde un lugar docente, como para ser ofrecido a los alumnos. Es un ambiente de generación de comunidad, de coexistencia, de construcción compartida.

Se acepta que es una forma poco habitual de enseñanza y aprendizaje en las futuras aulas de la Escuela Secundaria. Se acepta que es una forma desconocida. Más aún, aseguramos que será una tarea difícil el hacer que sea una forma comprendida de enseñanza y aprendizaje.

Finalmente, un aula Maturaniana será un aula más “productiva” y amigable... Un aula donde la ciencia, y todo el conocimiento, se construye a partir del Amor.

Un aforismos apropiado al tema: ***Todo ha sido escrito. Sin embargo, como pocos leen, habrá que reescribirlo.*** (oobravom)

En conclusión, así como para Feuerbach `Dios se hace a la manera del hombre, el ser se piensa a la manera del dasein`.
